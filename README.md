## Projeto Caixa Eletrônico
Projeto desenvolvido como teste técnico para a empresa Synapcom

## Requisitos técnicos e funcionais do projeto

1. Desenvolver em PHP ou Javascript;
2. Caixa deve entregar o menor número possível de notas;
3. Caixa deve validar se é possível sacar o valor solicitado com as notas disponíveis;
4. As notas disponíveis são: R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00;   

---

## Considerações

1. Não é necessário validar o saldo do cliente, para esse teste o saldo é infinito.
2. O valor solicitado será sempre positivo.
3. A quantidade de notas é infinita;
---

## Tecnologias Base Utilizadas
1. PHP 7.4.7
2. Composer 1.10.8

## Observações
Para testar, utilizando ambiente xampp com apache, coloque os arquivos do projeto no diretório xampp\htdocs\.
Caso queira testar com postman ao invés de usar a interface no navegador, basta usar a seguinte 
rota: http://localhost/caixaeletronico/api/sacar/{valor}, substituindo "{valor}" pelo valor desejado.