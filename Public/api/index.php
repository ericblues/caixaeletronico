<?php

use App\Controller\CaixaController;

require_once("../../vendor/autoload.php");

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$method = $_SERVER["REQUEST_METHOD"];
$uri = $_SERVER["REQUEST_URI"];

$controller = null;
$valor = null;
$unsetCount = 3;

//Trata a uri
$ex = explode("/", $uri);

for ($i = 0; $i < $unsetCount; $i++){
    unset($ex[$i]);
}

$ex = array_filter(array_values($ex));

if (isset($ex[0])){
    $controller = $ex[0];
}

if (isset($ex[1])){
    $valor = $ex[1];
}

$caixaController = new CaixaController();

switch ($method){
    case 'GET':
        if ($controller != null and $controller == 'sacar' && $valor != null){
            $resposta = $caixaController->sacar($valor);
            echo $resposta;
        }else{
            echo json_encode(["saque_disponivel" => false, "mensagem" => "Valor inválido!"]);
        }
        break;
    default:
        echo json_encode(["result" => "$method: requisicão inválida"]);
    break;
}