function sacar(){
    let inputValor = document.getElementById('inputValor')
    let valorSaque = inputValor.value

    const xmlhttp = new XMLHttpRequest()
    let url = `/caixaeletronico/api/sacar/${valorSaque}`

    xmlhttp.onreadystatechange = function(){
        if (this.readyState === 4 && this.status == 200){

            let resposta = JSON.parse(this.responseText.trim())
            abrirModal(resposta)
        }
    }

    xmlhttp.open("GET", url, true)
    xmlhttp.send()
}

function abrirModal(resposta){
    let modal = document.getElementById('myModal')
    let span = document.getElementsByClassName('close')[0]
    let txtModal = document.getElementById('txtModal')

    modal.style.display = "block"

    txtModal.innerHTML = ''

    if (resposta.saque_disponivel){
        let mensagem = '<ul>'

        resposta.cedulas.forEach(obj => {
            mensagem += '<li>'
            mensagem += `${obj.qtde} nota(s) de R\$ ${obj.cedula}`
            mensagem += '</li>'
        })

        mensagem += '</ul>'

        txtModal.innerHTML = mensagem
    }else{
        txtModal.innerHTML = `<span><b>Resposta</b>: ${resposta.mensagem}</span>`
    }

    span.onclick = function() {
        modal.style.display = "none"
    }

    window.onclick = function(event) {
        if (event.target == modal) {
          modal.style.display = "none"
        }
      }
}