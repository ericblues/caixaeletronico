<?php
namespace App\Entity;

class Cliente implements \JsonSerializable{

    private $temSaldo;

    public function __construct(){
        $this->temSaldo = true;
    }

    public function __get($atrib){
        return $this->$atrib;
    }

    public function __set($atrib, $value){
        $this->$atrib = $value;
    }

    public function jsonSerialize(){
        $vars = get_object_vars($this);

        return $vars;
    }
}