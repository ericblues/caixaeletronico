<?php
namespace App\Entity;

class Saque implements \JsonSerializable{

    private $cedula;
    private $qtde;

    public function __construct($cedula, $qtde){
        $this->cedula = $cedula;
        $this->qtde = $qtde;
    }

    public function __get($atrib){
        return $this->$atrib;
    }

    public function __set($atrib, $value){
        $this->$atrib = $value;
    }

    public function jsonSerialize(){
        $vars = get_object_vars($this);

        return $vars;
    }
}