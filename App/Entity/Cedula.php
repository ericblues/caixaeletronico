<?php
namespace App\Entity;

class Cedula implements \JsonSerializable{

    private $valor;

    public function __construct($valor){
        $this->valor = $valor;
    }

    public function __get($atrib){
        return $this->$atrib;
    }

    public function __set($atrib, $value){
        $this->$atrib = $value;
    }

    public function jsonSerialize(){
        $vars = get_object_vars($this);

        return $vars;
    }
}