<?php
namespace App\Model;

use App\Entity\Cedula;

class CaixaModel{

    private $arrayCedulas = [];

    public function __construct(){
        array_push($this->arrayCedulas, new Cedula(20));
        array_push($this->arrayCedulas, new Cedula(100));
        array_push($this->arrayCedulas, new Cedula(50));
        array_push($this->arrayCedulas, new Cedula(10));
    }

    public function listarCedulas(){
        $cedulas = array_map(function($cedula){
            return $cedula->valor;
        }, $this->arrayCedulas);

        sort($cedulas);
        
        return $cedulas;
    }
}