<?php
namespace App\Controller;

use App\Entity\Cliente;
use App\Entity\Saque;
use App\Model\CaixaModel;

class CaixaController{
    private $caixaModel;

    public function __construct(){
        $this->caixaModel = new CaixaModel();
    }

    public function sacar($valor){
        $resposta = ["saque_disponivel" => false, "cedulas" => [], "mensagem" => ""];
    
        $valorSaque = $this->validarValorSaque($valor);
        
        if ($valorSaque != false){
            if ($this->verificaDisponibilidadeSaque($valorSaque)){
                $valorRestante = $valorSaque;
                $cedulas = $this->caixaModel->listarCedulas();
               
                $resposta["saque_disponivel"] = true;
    
                while (count($cedulas) > 0){
                    $maiorCedulaCorrente = $this->maiorCedulaDisponivel($cedulas);
    
                    if ($valorSaque >= $maiorCedulaCorrente){
                        $qtdeNotas = intdiv($valorRestante, $maiorCedulaCorrente);
                        $valorRestante = $valorRestante % $maiorCedulaCorrente;
    
                        if ($qtdeNotas > 0){
                            array_push($resposta["cedulas"], new Saque($maiorCedulaCorrente, $qtdeNotas));
                        }
    
                    }
    
                    array_pop($cedulas);
                }

                $resposta["mensagem"] = "Saque disponível, cédulas separadas com sucesso!";        
            }else{
                $resposta["saque_disponivel"] = false;
                $resposta["mensagem"] = "Valor não disponível para saque, cédulas insuficientes!";
            }
        }else{
            $resposta["saque_disponivel"] = false;
            $resposta["mensagem"] = "Valor inválido para saque, tipo inválido para: $valor!";
        }
        

        return json_encode($resposta);
    }

    private function menorCedulaDisponivel($cedulas){
        return min($cedulas);
    }

    private function maiorCedulaDisponivel($cedulas){
        return max($cedulas);
    }

    private function validarValorSaque($valor){
        $valorSaque = filter_var($valor, FILTER_VALIDATE_INT);

        return $valorSaque;
    }

    private function verificaDisponibilidadeSaque($valorSaque){
        $cliente = new Cliente();

        if ($cliente->temSaldo){
            $arrayCedulas = $this->caixaModel->listarCedulas();
          
            $menorCedula = $this->menorCedulaDisponivel($arrayCedulas);

            if ($valorSaque >= $menorCedula){
                if (($valorSaque % $menorCedula) == 0){
                    return true;
                }else{
                    return false;
                }
            }
        }

        return false;
    }
}